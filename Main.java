package dnd;

import java.util.Scanner;
import java.math.*;
import java.util.Random;

public class Main {
	static int playerHp = 100;
	static int shield = 20;
	static int enemyHp = 100;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);

		while (enemyHp > 0 && playerHp > 0) {

			// Options for the player to be printed on screen
			System.out.println("You are being attacked by a cactus! Beware!");
			System.out.println();
			System.out.println();
			System.out.println("Your health: " + playerHp);
			System.out.println("Enemies health: " + enemyHp);
			System.out.println();
			System.out.println("Please choose your action! Type the number!");
			System.out.println("1. Attack");
			System.out.println("2. Defend");
			System.out.println("3. Quit Game");

			int option = scanner.nextInt();
			if (option == 1) {
				attack();
				continue;

			} else if (option == 2) {
				defend();
				continue;

			} else if (option == 3) {
				quit();
				break;

			}

		}
		if (enemyHp <= 0) {
			System.out.println("You have defeated the enemy");
		} else if (playerHp <= 0) {
			System.out.println("You have been defeated");
		}

	}

	public static void attack() {
		int sword = new Random().nextInt(20);
		int enemySword = new Random().nextInt(15);
		if (sword == 0) {
			System.out.println("You missed!");
		} else {

			System.out.println("You attacked");
			System.out.println("You did " + sword + " damage.");
			enemyHp = enemyHp - sword;
		}

		if (enemySword == 0) {
			System.out.println("The enemey missed!");
		} else {
			System.out.println("The enemy attacked");
			System.out.println("The enemy did " + enemySword + " damage");
			playerHp = playerHp - enemySword;
		}

	}

	// TODO implement further methods
	public static void defend() {
		System.out.println("You defended");
		int enemySword = new Random().nextInt(15);
		if (enemySword == 0) {
			System.out.println("The enemy missed");
		} else if ((enemySword - shield) < 0) {
			System.out.println("You staggered the enemy attack and launched a counter attack");
			int sword = new Random().nextInt(20);
			System.out.println("You did " + sword + " damage.");
			enemyHp = enemyHp - sword;
		}

		else {
			System.out.println("The enemy attacked with " + enemySword + " damage");
			System.out.println("Your shield blocked " + shield + " damage");
			playerHp = playerHp - (enemySword - shield);
		}

	}

	public static void quit() {
		System.out.println("Thanks for playing!");
	}

}

